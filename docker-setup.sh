#!/bin/bash

# To the extent possible under law, Marko Dimjašević
# (https://dimjasevic.net/marko) has waived all copyright and related
# or neighboring rights to this script.

# The script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# This script assumes a Debian Jessie 64-bit installation

set -e

# Configure a language

source locale.sh

# Install Docker dependencies from stable repositories
sudo apt-get install --assume-yes aufs-tools cgroupfs-mount git git-man libapparmor1 liberror-perl rsync

# Add a Debian unstable repo for Docker and install it
src_file=/tmp/sources.list
dest_file=/etc/apt/sources.list

cp ${dest_file} ${src_file}

sudo echo "deb http://mirrors.kernel.org/debian unstable main" >> ${src_file}
sudo echo "deb-src http://mirrors.kernel.org/debian unstable main" >> ${src_file}

sudo mv ${dest_file} ${dest_file}.old
sudo mv ${src_file} ${dest_file}

sudo apt-get update
sudo apt-get install --assume-yes docker.io/unstable


# Add the user to the docker group such that no sudo is needed all the time
sudo usermod -aG docker $USER
