#!/bin/bash

# To the extent possible under law, Marko Dimjašević
# (https://dimjasevic.net/marko) has waived all copyright and related
# or neighboring rights to this script.

# The script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# This script assumes a Debian Jessie 64-bit installation

set -e

# Configure a language

source locale.sh

# Vagrant and its dependencies to make it work with libvirt

sudo apt-get install --assume-yes \
    vagrant libxslt1-dev libxml2-dev libvirt-dev nfs-kernel-server zlib1g-dev


# Install a few Vagrant plugins: vagrant-libvirt (which needs
# fog-libvirt) and vagrant-mutate.

vagrant plugin install fog-libvirt --plugin-version=0.0.3
vagrant plugin install vagrant-libvirt
vagrant plugin install vagrant-mutate

# Optionally, you might want to install another plugin called
# vagrant-cachier, which caches packages downloaded by boxes

vagrant plugin install vagrant-cachier
