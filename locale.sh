#!/bin/bash

# To the extent possible under law, Marko Dimjašević
# (https://dimjasevic.net/marko) has waived all copyright and related
# or neighboring rights to this script.

# The script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# This script assumes a clean Debian 8 64-bit installation

# Configure a language

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
sudo locale-gen en_US.UTF-8
sudo dpkg-reconfigure -f noninteractive locales
